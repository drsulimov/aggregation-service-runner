package com.cgi.aggregationservicerunner.repositories;

import com.cgi.aggregationservicerunner.entities.JiraTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JiraTicketRepository extends JpaRepository<JiraTicket, Long> {
}
