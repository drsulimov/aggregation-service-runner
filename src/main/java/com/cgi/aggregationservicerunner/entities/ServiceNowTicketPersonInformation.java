package com.cgi.aggregationservicerunner.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Details about the ServiceNow ticket and extended information about the person")
public class ServiceNowTicketPersonInformation {
    @ApiModelProperty(notes = "The ServiceNow Ticket")
    private ServiceNowTicket serviceNowTicket;

    @ApiModelProperty(notes = "The extended information about the person")
    private PersonInformation personInformation;
}
