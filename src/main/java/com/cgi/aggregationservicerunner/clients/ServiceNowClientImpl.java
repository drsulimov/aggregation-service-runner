package com.cgi.aggregationservicerunner.clients;

import com.cgi.aggregationservicerunner.configs.HttpAuthorizationPreparer;
import com.cgi.aggregationservicerunner.entities.PersonInformation;
import com.cgi.aggregationservicerunner.entities.ServiceNowTicket;
import com.cgi.aggregationservicerunner.entities.ServiceNowTicketPersonInformation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ServiceNowClientImpl implements ServiceNowClient {
    private RestTemplate restTemplate;
    private HttpAuthorizationPreparer preparedRequest;

    @Autowired
    public ServiceNowClientImpl(RestTemplate restTemplate, HttpAuthorizationPreparer preparedRequest) {
        this.restTemplate = restTemplate;
        this.preparedRequest = preparedRequest;
    }

    @Override
    public ServiceNowTicket getServiceNowTicketById(Long id) {
        ResponseEntity<ServiceNowTicket> response = restTemplate.getForEntity(
                "https://cgi-servicenow.herokuapp.com/api/v1/tickets/{id}",
                ServiceNowTicket.class,
                id);
        return response.getBody();
    }

    @Override
    public ServiceNowTicket[] getAllServiceNowTickets(int offset, int limit) {
        ResponseEntity<ServiceNowTicket[]> response = restTemplate.getForEntity(
                "https://cgi-servicenow.herokuapp.com/api/v1/tickets?offset={offset}&limit={limit}",
                ServiceNowTicket[].class,
                offset,
                limit);
        return response.getBody();
    }

    @Override
    public ServiceNowTicketPersonInformation getServiceNowTicketAndPersonInformationByIdPersonCreator(String auth, Long id) {
        ServiceNowTicketPersonInformation serviceNowTicketPersonInformation = new ServiceNowTicketPersonInformation();
        ServiceNowTicket serviceNowTicket = getServiceNowTicketById(id);
        serviceNowTicketPersonInformation.setServiceNowTicket(serviceNowTicket);
        ResponseEntity<PersonInformation> response = restTemplate.exchange(
                "https://cgi-user-management.herokuapp.com/api/v1/extended-information/persons/{id}",
                HttpMethod.GET,
                preparedRequest.authorizationRequest(auth),
                PersonInformation.class,
                serviceNowTicket.getIdPersonCreator());
        serviceNowTicketPersonInformation.setPersonInformation(response.getBody());
        return serviceNowTicketPersonInformation;
    }
}
