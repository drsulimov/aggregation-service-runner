package com.cgi.aggregationservicerunner.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@ApiModel(description = "Details about the Jira ticket")
public class JiraTicket implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the Jira ticket")
    private Long id;

    @ApiModelProperty(notes = "The Jira ticket's name")
    private String name;

    @ApiModelProperty(notes = "The person's email who created a Jira ticket")
    private String email;

    @ApiModelProperty(notes = "The person's unique id who created a Jira ticket")
    private Long idPersonCreator;

    @ApiModelProperty(notes = "The person's unique id who assigned to provided Jira ticket")
    private Long idPersonAssigned;

    @ApiModelProperty(notes = "The date and time when Jira ticket was created")
    private Timestamp creationDatetime;

    @ApiModelProperty(notes = "The date and time when Jira ticket was closed")
    private Timestamp ticketCloseDatetime;
}
