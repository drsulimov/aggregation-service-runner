package com.cgi.aggregationservicerunner.entities;

import lombok.Data;

import java.util.List;

@Data
public class PersonInformation {
    private Person person;
    private Address address;
    private List<Group> groups;
    private List<Role> roles;
}
