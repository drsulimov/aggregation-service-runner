package com.cgi.aggregationservicerunner.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(description = "Details about the Jira ticket and extended information about the person")
public class JiraTicketPersonInformation {
    @ApiModelProperty(notes = "The Jira Ticket")
    private JiraTicket jiraTicket;

    @ApiModelProperty(notes = "The extended information about the person")
    private PersonInformation personInformation;
}
