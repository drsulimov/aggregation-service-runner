package com.cgi.jira.api;

import com.cgi.aggregationservicerunner.handlers.ValidEmail;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class JiraTicketDto implements Serializable {
    private Long id;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[A-Z]+[0-9]{5}")
    private String name;

    @NotNull
    @NotEmpty
    @ValidEmail
    @Length(max = 100)
    private String email;

    @NotNull
    private Long idPersonCreator;

    @NotNull
    private Long idPersonAssigned;

    private Timestamp creationDatetime;
    private Timestamp ticketCloseDatetime;
}
