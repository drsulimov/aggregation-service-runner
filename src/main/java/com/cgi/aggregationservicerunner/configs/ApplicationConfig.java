package com.cgi.aggregationservicerunner.configs;

import com.cgi.aggregationservicerunner.clients.JiraClientImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ApplicationConfig {
    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(new ObjectMapper());
        restTemplate.getMessageConverters().add(converter);
        return restTemplate;
    }

    @Bean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("com.cgi.aggregationservicerunner.wsdl");
        return marshaller;
    }

    @Bean
    public JiraClientImpl jiraClient(Jaxb2Marshaller marshaller) {
        JiraClientImpl client = new JiraClientImpl();
        client.setDefaultUri("https://cgi-jira.herokuapp.com/ws");
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);
        return client;
    }

    @Bean
    public HttpAuthorizationPreparer preparedRequest() {
        return new HttpAuthorizationPreparer();
    }
}
