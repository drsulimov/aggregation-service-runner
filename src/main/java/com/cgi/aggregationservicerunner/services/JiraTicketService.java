package com.cgi.aggregationservicerunner.services;

import com.cgi.aggregationservicerunner.entities.JiraTicket;
import com.cgi.aggregationservicerunner.repositories.JiraTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JiraTicketService {
    private JiraTicketRepository jiraTicketRepository;

    @Autowired
    public JiraTicketService(JiraTicketRepository jiraTicketRepository) {
        this.jiraTicketRepository = jiraTicketRepository;
    }

    public JiraTicket saveJiraTicket(JiraTicket ticket) {
        return jiraTicketRepository.save(ticket);
    }
}
