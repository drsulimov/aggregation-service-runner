package com.cgi.aggregationservicerunner.clients;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class MessageReceiver {
    @RabbitListener(queues = "messages")
    public void listen(String message) {
        System.out.println("Message: " + message);
    }
}
