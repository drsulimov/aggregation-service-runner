package com.cgi.aggregationservicerunner.clients;

import com.cgi.aggregationservicerunner.entities.ServiceNowTicket;
import com.cgi.aggregationservicerunner.entities.ServiceNowTicketPersonInformation;

public interface ServiceNowClient {
    ServiceNowTicket getServiceNowTicketById(Long id);

    ServiceNowTicket[] getAllServiceNowTickets(int offset, int limit);

    ServiceNowTicketPersonInformation getServiceNowTicketAndPersonInformationByIdPersonCreator(String auth, Long id);
}
