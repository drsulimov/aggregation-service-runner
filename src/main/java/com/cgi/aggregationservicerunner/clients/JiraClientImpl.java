package com.cgi.aggregationservicerunner.clients;

import com.cgi.aggregationservicerunner.configs.HttpAuthorizationPreparer;
import com.cgi.aggregationservicerunner.entities.JiraTicket;
import com.cgi.aggregationservicerunner.entities.JiraTicketPersonInformation;
import com.cgi.aggregationservicerunner.entities.PersonInformation;
import com.cgi.aggregationservicerunner.wsdl.GetTicketByIdRequest;
import com.cgi.aggregationservicerunner.wsdl.GetTicketByIdResponse;
import com.cgi.aggregationservicerunner.wsdl.TicketById;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import java.sql.Timestamp;

public class JiraClientImpl extends WebServiceGatewaySupport implements JiraClient {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private HttpAuthorizationPreparer preparedRequest;

    @Override
    public JiraTicket getJiraTicketById(Long id) {
        GetTicketByIdRequest request = new GetTicketByIdRequest();
        request.setId(id);
        GetTicketByIdResponse response = (GetTicketByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);
        return jiraTicketMapper(response.getTicketById());
    }

    @Override
    public JiraTicketPersonInformation getJiraTicketAndPersonInformationByIdPersonCreator(String auth, Long id) {
        JiraTicketPersonInformation jiraTicketPersonInformation = new JiraTicketPersonInformation();
        GetTicketByIdRequest request = new GetTicketByIdRequest();
        request.setId(id);
        GetTicketByIdResponse response = (GetTicketByIdResponse) getWebServiceTemplate().marshalSendAndReceive(request);
        TicketById ticket = response.getTicketById();
        jiraTicketPersonInformation.setJiraTicket(jiraTicketMapper(ticket));
        ResponseEntity<PersonInformation> responseEntity = restTemplate.exchange(
                "https://cgi-user-management.herokuapp.com/api/v1/extended-information/persons/{id}",
                HttpMethod.GET,
                preparedRequest.authorizationRequest(auth),
                PersonInformation.class,
                ticket.getIdPersonCreator());
        jiraTicketPersonInformation.setPersonInformation(responseEntity.getBody());
        return jiraTicketPersonInformation;
    }

    private JiraTicket jiraTicketMapper(TicketById ticket) {
        JiraTicket jiraTicket = new JiraTicket();
        jiraTicket.setId(ticket.getId());
        jiraTicket.setName(ticket.getName());
        jiraTicket.setEmail(ticket.getEmail());
        jiraTicket.setIdPersonCreator(ticket.getIdPersonCreator());
        jiraTicket.setIdPersonAssigned(ticket.getIdPersonAssigned());
        jiraTicket.setCreationDatetime(new Timestamp(ticket.getCreationDatetime().toGregorianCalendar().getTimeInMillis()));
        jiraTicket.setTicketCloseDatetime(new Timestamp(ticket.getTicketCloseDatetime().toGregorianCalendar().getTimeInMillis()));
        return jiraTicket;
    }
}
