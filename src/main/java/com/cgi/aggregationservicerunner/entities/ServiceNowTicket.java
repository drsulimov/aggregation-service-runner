package com.cgi.aggregationservicerunner.entities;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@Entity
@ApiModel(description = "Details about the ServiceNow ticket")
public class ServiceNowTicket implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @ApiModelProperty(notes = "The unique id of the ServiceNow ticket")
    private Long id;

    @ApiModelProperty(notes = "The ServiceNow ticket's name")
    private String name;

    @ApiModelProperty(notes = "The person's email who created a ServiceNow ticket")
    private String email;

    @ApiModelProperty(notes = "The person's unique id who created a ServiceNow ticket")
    private Long idPersonCreator;

    @ApiModelProperty(notes = "The person's unique id who assigned to provided ServiceNow ticket")
    private Long idPersonAssigned;

    @ApiModelProperty(notes = "The date and time when ServiceNow ticket was created")
    private Timestamp creationDatetime;
}
