package com.cgi.aggregationservicerunner.clients;

import com.cgi.aggregationservicerunner.entities.JiraTicket;
import com.cgi.aggregationservicerunner.mappers.BeanMapping;
import com.cgi.aggregationservicerunner.services.JiraTicketService;
import com.cgi.jira.api.JiraTicketDto;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Component
public class JiraTicketReceiver {
    private JiraTicketService jiraTicketService;
    private BeanMapping beanMapping;

    @Autowired
    public JiraTicketReceiver(JiraTicketService jiraTicketService, BeanMapping beanMapping) {
        this.jiraTicketService = jiraTicketService;
        this.beanMapping = beanMapping;
    }

    @RabbitListener(queues = "jira_tickets")
    public void listener(@Valid JiraTicketDto ticket) {
        jiraTicketService.saveJiraTicket(beanMapping.mapTo(ticket, JiraTicket.class));
    }
}
