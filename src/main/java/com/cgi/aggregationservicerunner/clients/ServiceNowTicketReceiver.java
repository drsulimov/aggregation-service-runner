package com.cgi.aggregationservicerunner.clients;

import com.cgi.aggregationservicerunner.entities.ServiceNowTicket;
import com.cgi.aggregationservicerunner.mappers.BeanMapping;
import com.cgi.aggregationservicerunner.services.ServiceNowTicketService;
import com.cgi.servicenow.api.ServiceNowTicketDto;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;

@Component
public class ServiceNowTicketReceiver {
    private ServiceNowTicketService serviceNowTicketService;
    private BeanMapping beanMapping;

    @Autowired
    public ServiceNowTicketReceiver(ServiceNowTicketService serviceNowTicketService, BeanMapping beanMapping) {
        this.serviceNowTicketService = serviceNowTicketService;
        this.beanMapping = beanMapping;
    }

    @RabbitListener(queues = "servicenow_tickets")
    public void receiveServiceNowTicket(@Valid ServiceNowTicketDto ticket) {
        serviceNowTicketService.saveServiceNowTicket(beanMapping.mapTo(ticket, ServiceNowTicket.class));
    }
}
