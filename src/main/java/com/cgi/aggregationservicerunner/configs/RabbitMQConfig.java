package com.cgi.aggregationservicerunner.configs;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    @Bean
    public Queue serviceNowTicketsQueue() {
        return new Queue("servicenow_tickets", false);
    }

    @Bean
    public Queue jiraTicketsQueue() {
        return new Queue("jira_tickets", false);
    }
}
