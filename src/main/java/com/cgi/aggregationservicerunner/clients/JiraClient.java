package com.cgi.aggregationservicerunner.clients;

import com.cgi.aggregationservicerunner.entities.JiraTicket;
import com.cgi.aggregationservicerunner.entities.JiraTicketPersonInformation;

public interface JiraClient {
    JiraTicket getJiraTicketById(Long id);

    JiraTicketPersonInformation getJiraTicketAndPersonInformationByIdPersonCreator(String auth, Long id);
}
