package com.cgi.aggregationservicerunner.controllers;

import com.cgi.aggregationservicerunner.clients.JiraClient;
import com.cgi.aggregationservicerunner.clients.ServiceNowClient;
import com.cgi.aggregationservicerunner.entities.JiraTicket;
import com.cgi.aggregationservicerunner.entities.JiraTicketPersonInformation;
import com.cgi.aggregationservicerunner.entities.ServiceNowTicket;
import com.cgi.aggregationservicerunner.entities.ServiceNowTicketPersonInformation;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/v1/tickets")
public class TicketController {
    private JiraClient jiraClient;
    private ServiceNowClient serviceNowClient;

    @Autowired
    public TicketController(JiraClient jiraClient, ServiceNowClient serviceNowClient) {
        this.jiraClient = jiraClient;
        this.serviceNowClient = serviceNowClient;
    }

    @GetMapping("/jira/{id}")
    @ApiOperation(
            value = "Finds Jira ticket by id",
            notes = "Provide an id to look up specific Jira ticket",
            response = JiraTicket.class)
    public ResponseEntity<JiraTicket> getJiraTicketById(
            @ApiParam(
                    value = "Id value for retrieving the Jira ticket",
                    required = true) @PathVariable Long id) {
        return new ResponseEntity<>(jiraClient.getJiraTicketById(id), HttpStatus.OK);
    }

    @GetMapping("/jira-person/{id}")
    @ApiOperation(
            value = "Finds Jira Ticket with Person Information by id",
            notes = "Provide an id to look up specific Jira ticket and extended information about the person",
            response = JiraTicketPersonInformation.class)
    public ResponseEntity<JiraTicketPersonInformation> getJiraTicketAndPersonInformationByIdPersonCreator(
            @RequestHeader(value = "Authorization") @ApiIgnore String auth,
            @ApiParam(
                    value = "Id value for retrieving the Jira ticket and extended information about the person",
                    required = true) @PathVariable Long id) {
        return new ResponseEntity<>(jiraClient.getJiraTicketAndPersonInformationByIdPersonCreator(auth, id), HttpStatus.OK);
    }

    @GetMapping("/servicenow/{id}")
    @ApiOperation(
            value = "Finds ServiceNow ticket by id",
            notes = "Provide an id to look up specific ServiceNow ticket",
            response = ServiceNowTicket.class)
    public ResponseEntity<ServiceNowTicket> getServiceNowTicketById(
            @ApiParam(
                    value = "Id value for retrieving the ServiceNow ticket",
                    required = true) @PathVariable Long id) {
        return new ResponseEntity<>(serviceNowClient.getServiceNowTicketById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/servicenow", params = {"offset", "limit"}, method = RequestMethod.GET)
    @ApiOperation(
            value = "Find all ServiceNow tickets by id",
            notes = "Provide an id to find all ServiceNow tickets using pagination",
            response = ServiceNowTicket[].class)
    public ResponseEntity<ServiceNowTicket[]> getAllServiceNowTickets(
            @ApiParam(
                    value = "Offset value for show current page",
                    required = true) @RequestParam int offset,
            @ApiParam(
                    value = "Limit value for set ticket's limit on the page",
                    required = true) @RequestParam int limit) {
        return new ResponseEntity<>(serviceNowClient.getAllServiceNowTickets(offset, limit), HttpStatus.OK);
    }

    @GetMapping("/servicenow-person/{id}")
    @ApiOperation(value = "Finds ServiceNow Ticket with Person Information by id",
            notes = "Provide an id to look up specific ServiceNow ticket and extended information about the person",
            response = ServiceNowTicketPersonInformation.class)
    public ResponseEntity<ServiceNowTicketPersonInformation> getServiceNowTicketAndPersonInformationByIdPersonCreator(
            @RequestHeader(value = "Authorization") @ApiIgnore String auth,
            @ApiParam(
                    value = "Id value for retrieving the ServiceNow ticket and extended information about the person",
                    required = true) @PathVariable Long id) {
        return new ResponseEntity<>(serviceNowClient.getServiceNowTicketAndPersonInformationByIdPersonCreator(auth, id), HttpStatus.OK);
    }
}
