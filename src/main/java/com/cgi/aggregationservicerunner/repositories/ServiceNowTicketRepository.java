package com.cgi.aggregationservicerunner.repositories;

import com.cgi.aggregationservicerunner.entities.ServiceNowTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServiceNowTicketRepository extends JpaRepository<ServiceNowTicket, Long> {
}
