package com.cgi.aggregationservicerunner.configs;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;

public class HttpAuthorizationPreparer {
    public HttpAuthorizationPreparer() {
    }

    public HttpEntity<String> authorizationRequest(String encodedCredentials) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", encodedCredentials);
        return new HttpEntity<>(headers);
    }
}
