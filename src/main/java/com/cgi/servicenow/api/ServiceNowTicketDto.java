package com.cgi.servicenow.api;

import com.cgi.aggregationservicerunner.handlers.ValidEmail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.Instant;

@Getter
@Setter
@ToString
public class ServiceNowTicketDto implements Serializable {
    private Long id;

    @NotNull
    @NotEmpty
    @Pattern(regexp = "[A-Z]+[0-9]{5}")
    private String name;

    @NotNull
    @NotEmpty
    @ValidEmail
    @Length(max = 100)
    private String email;

    @NotNull
    private Long idPersonCreator;

    @NotNull
    private Long idPersonAssigned;

    private Timestamp creationDatetime;

    public ServiceNowTicketDto() {
        this.creationDatetime = new Timestamp(Instant.now().toEpochMilli());
    }

    public ServiceNowTicketDto(Long id,
                               String name,
                               String email,
                               Long idPersonCreator,
                               Long idPersonAssigned,
                               Timestamp creationDatetime) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.idPersonCreator = idPersonCreator;
        this.idPersonAssigned = idPersonAssigned;
        this.creationDatetime = creationDatetime;
    }
}

