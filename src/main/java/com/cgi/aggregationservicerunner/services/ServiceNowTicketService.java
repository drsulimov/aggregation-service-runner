package com.cgi.aggregationservicerunner.services;

import com.cgi.aggregationservicerunner.entities.ServiceNowTicket;
import com.cgi.aggregationservicerunner.repositories.ServiceNowTicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ServiceNowTicketService {
    private ServiceNowTicketRepository serviceNowTicketRepository;

    @Autowired
    public ServiceNowTicketService(ServiceNowTicketRepository serviceNowTicketRepository) {
        this.serviceNowTicketRepository = serviceNowTicketRepository;
    }

    public ServiceNowTicket saveServiceNowTicket(ServiceNowTicket ticket) {
        return serviceNowTicketRepository.save(ticket);
    }
}
